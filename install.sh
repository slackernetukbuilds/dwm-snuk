#!/bin/bash
#
# simple script to configure DWM
#
cd $(dirname $0) ; CWD=$(pwd)

if [ ! -d $HOME/bin ]; then
	mkdir $HOME/bin	
fi

if [ ! -d $HOME/.config/alacritty ]; then
	mkdir -p $HOME/.config/alacritty || exit 1
fi

if [ ! -d $HOME/.fonts ]; then
	mkdir -p $HOME/.fonts
fi

if [ ! -d $HOME/.vim/colors ]; then
	mkdir -p $HOME/.vim/colors
fi


src=(
	bar	
	rofi_run
)

dotf=( 	.Xresources  
	.bash_profile  
	.bashrc  
	.vimrc  
	.xprofile 
)	

for i in ${src[@]}; do
	install -m0755 $CWD/dwmblocks-scripts/$i -t $HOME/bin/ || exit 1	
done


install -m644 $CWD/dots/alacritty.yml -t $HOME/.config/alacritty || exit 1


cp -a $CWD/fonts/* $HOME/.fonts/ || exit 1


install -m644 $CWD/dots/molokai.vim -t $HOME/.vim/colors/ || exit 1

for df in ${dotf[@]}; do
	install -m644 $CWD/dots/$df -t $HOME/ || exit 1
done

cp -a $CWD/wallpaper $HOME || exit 1

fc-cache -f
