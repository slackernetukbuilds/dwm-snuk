export LANG=en_GB.UTF-8
export LC_COLLATE=C
export MESA_LOADER_DRIVER_OVERRIDE=i965
export BROWSER="google-chrome-stable"
PATH=$PATH:$HOME/bin:$HOME/.local/bin
