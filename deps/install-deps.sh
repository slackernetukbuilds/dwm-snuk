#!/bin/bash
#
cp ./alacritty-deps/ndk-sys-0.4.1+23.1.7779620.crate \
	/var/cache/sbopkg || exit 1
cp ./alacritty-deps/wasi-0.11.0+wasi-snapshot-preview1.crate \
	/var/cache/sbopkg || exit 1
sbopkg=(
	rust16
	alacritty	
	imlib2
	giblib
	scrot
	libconfig
	compton
	feh
	xcb-util-xrm
	rofi
)

snuk=(
	Font-Awesome 
)

for i1 in ${sbopkg[@]}; do
	sbopkg -B -i $i1 || exit 1	
done

for i2 in ${snuk[@]}; do
	cd $i2 || exit 1
	sh $i2.SlackBuild || exit 1
	cd ..
done

sh latest-chrome.sh --install || exit 1

install -m755 ./xinitrc.DWM -t /etc/X11/xinit/ || exit 1
exit 0
