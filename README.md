<h1>dwm - dynamic window manager on Slackware</h1>

![dwm-screenshot](/uploads/e991eaef410c7ca5cc826d9338540f7d/dwm-screenshot.png)


1. Firstly we need to download,install and configure sbopkg to install some dependencies.

```bash
wget https://github.com/sbopkg/sbopkg/releases/download/0.38.2/sbopkg-0.38.2-noarch-1_wsr.tgz
```
```bash
installpkg sbopkg-0.38.2-noarch-1_wsr.tgz
```
The default configuration for Sbopkg based on Slackware 15.0, if you install Sbopkg on Slackware current,
we need to modify the /etc/sbopkg/sbopkg.conf configuration file.

```bash
REPO_BRANCH=${REPO_BRANCH:-current}
```
```bash
REPO_NAME=${REPO_NAME:-SBo-git}
```

then run sbopkg as root with the '-r' (remote sync) flag.

```bash
sbopkg -r 
```

2. Next step, we have to install all dependencies as root of course.

```bash
cd deps
sh install-deps.sh
```

3. build and install dwm and dwmblocks
```bash
cd core/dwm
```
```bash
make clean install
```
```bash
cd core/dwmblocks
```
```bash
make clean install 
```

4. We going to configure our dwm - dynamic window manager as user.
   
   **_You should consider to backup your dotfiles, .bashrc, .bash_profiles, .Xresources etc are overritten._**

```bash
sh install.sh
```

5. Finally

Add to /etc/slackpkg/blacklist file

```bash
[0-9]+_snuk
```
My keyboard layout is configured for british-english.<br>
**_$HOME/.xprofiles_**
```bash
setxkbmap -model pc105 -layout gb &
```
If you would like to change to an US layout - just an example

```bash
setxkbmap -model pc104 -layout us &
```

Configuration
Add to /etc/slackpkg/blacklist file
```bash
[0-9]+_snuk
```
xwmconfig to setup DWM as standard-session


